package mx.unam.fciencias.moviles.oikos.bird_proyecto;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends Navegacion {

    FloatingActionButton epocaSeleccionada; //Representa el boton de cada epoca
    private DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main); //Muestra el contenido de la pantalla principal
        epocaSeleccionada= findViewById(R.id.E1);   //Busca la opcion seleccionada
        epocaSeleccionada.setOnClickListener(this::mostrarEpoca);   //Comportamiento al presionar el boton de la epoca.

        toolbar = findViewById(R.id.toolbar);// Apartado para mostar el menu despegable
        setSupportActionBar(findViewById(R.id.toolbar)); //Mantiene registro de actividad del menu
        drawerLayout = findViewById(R.id.drawer_layout);    //Carga los elementos del menu despegable
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close); //Obtiene el momento
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();



    }


    /**
     * Método para mostrar la pantalla de epoca seleccionada en respuesta al boton correspondiente
     * */
    public void mostrarEpoca(View botonEpoca){

        Intent intent= new Intent(this, Epoca.class);
        startActivity(intent);
    }


}