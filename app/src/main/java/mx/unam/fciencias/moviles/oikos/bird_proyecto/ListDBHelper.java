package mx.unam.fciencias.moviles.oikos.bird_proyecto;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.crypto.Cipher;

import mx.unam.fciencias.moviles.oikos.bird_proyecto.provider.Contract;

public class ListDBHelper extends SQLiteOpenHelper {
    public static final byte DATABASE_VERSION = 1;

    //Base de datos para almacenar las epocas con sus respectivos personajes
    public static final String DATABASE_NAME = "EPOCA_PERSONAJES";

    // Herramienta de apoyo para poder hacer consultas en la base de datos.

    public ListDBHelper(Context context){
        super(context,DATABASE_NAME+".db",null,DATABASE_VERSION);
    }

    /**
     * Metodo para poder crear la tabla usando la sintaxis de sql
     * */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        StringBuilder createQuery = new StringBuilder();
        createQuery.append("CREATE TABLE IF NOT EXISTS").append("(")
                .append(Contract.ListaEpocasPersonajes._ID)
                .append("INTEGER PRIMARY KEY DEFAULT 1, ")
                .append(Contract.ListaEpocasPersonajes.COLUMN_LIST_SIZE)
                .append("INTEGER CHECK(")
                .append(Contract.ListaEpocasPersonajes.COLUMN_LIST_SIZE)
                .append(">0);");
        sqLiteDatabase.execSQL(createQuery.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1){

    }

}
