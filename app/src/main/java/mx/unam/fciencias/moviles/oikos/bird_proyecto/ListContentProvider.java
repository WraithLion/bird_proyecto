package mx.unam.fciencias.moviles.oikos.bird_proyecto;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import mx.unam.fciencias.moviles.oikos.bird_proyecto.provider.Contract;

public class ListContentProvider extends ContentProvider {

    private ListDBHelper dbHelper;
    public static final byte LIST_SIZE=100;

    public static final byte LIST_SIZE_WITH_ID=101;

    private static final UriMatcher CONTENT_MATCHER;

    static {

        CONTENT_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        CONTENT_MATCHER.addURI(Contract.AUTHORITY,Contract.ListaEpocasPersonajes.PATH_INFINITE_LIST,LIST_SIZE);
        CONTENT_MATCHER.addURI(Contract.AUTHORITY,Contract.ListaEpocasPersonajes.PATH_INFINITE_LIST+"#",LIST_SIZE_WITH_ID);

    }

    @Override
    public boolean onCreate(){
        dbHelper=new ListDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder){

        SQLiteDatabase db= dbHelper.getReadableDatabase();
        Cursor result;
        switch(CONTENT_MATCHER.match(uri)){
            case LIST_SIZE:
                result = db.query(Contract.ListaEpocasPersonajes.TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
                //TODO
                break;
            case LIST_SIZE_WITH_ID:
                result=db.query(Contract.ListaEpocasPersonajes.TABLE_NAME,projection, Contract.ListaEpocasPersonajes._ID+"= ?",new String [] {uri.getPathSegments().get(1)},null,null,null);
                break;
            default:
                throw new UnsupportedOperationException("Uri no reconocida: "+ uri);

        }
        Context context = getContext();
        if (context!= null){
            result.setNotificationUri(context.getContentResolver(),uri);
        }
        return result;
    }



    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues){
        SQLiteDatabase db= dbHelper.getWritableDatabase();
        long newId;
        switch (CONTENT_MATCHER.match(uri)){
            case LIST_SIZE:
                newId= db.insert(Contract.ListaEpocasPersonajes.TABLE_NAME,null, contentValues);
                break;
            case LIST_SIZE_WITH_ID:
                if (contentValues== null){
                    contentValues= new ContentValues();
                }
                contentValues.put(Contract.ListaEpocasPersonajes._ID,uri.getPathSegments().get(1));
                newId= db.insert(Contract.ListaEpocasPersonajes.TABLE_NAME,null, contentValues);
                break;
            default:
                throw new UnsupportedOperationException("URI no reconocida: "+ uri);
        }
        if (newId<=0){
            throw new SQLException("No fue posible insertar");
        }
        return ContentUris.withAppendedId(Contract.ListaEpocasPersonajes.CONTENT_URI,newId);

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs){
        SQLiteDatabase db= dbHelper.getWritableDatabase();
        int entradasEliminadas;
        switch(CONTENT_MATCHER.match(uri)){
            case LIST_SIZE:
                entradasEliminadas=db.delete(Contract.ListaEpocasPersonajes.TABLE_NAME,selection,selectionArgs);
                break;
            case LIST_SIZE_WITH_ID:
                entradasEliminadas= db.delete(Contract.ListaEpocasPersonajes.TABLE_NAME,Contract.ListaEpocasPersonajes._ID + "= ?",new String[] {uri.getPathSegments().get(1)});
                break;
            default:
                throw new UnsupportedOperationException("Borrado fallido en: "+ uri);
        }
        Context context = getContext();
        if (context!=null&& entradasEliminadas>0){
context.getContentResolver().notifyChange(uri,null);

        }
        return entradasEliminadas;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int entradasActualizadas;
        switch (CONTENT_MATCHER.match(uri)){
            case LIST_SIZE:
                entradasActualizadas=db.update(Contract.ListaEpocasPersonajes.TABLE_NAME, contentValues,selection,selectionArgs);
                break;
            case LIST_SIZE_WITH_ID:
                entradasActualizadas= db.update(Contract.ListaEpocasPersonajes.TABLE_NAME,contentValues,Contract.ListaEpocasPersonajes._ID+"=?",new String[] {uri.getPathSegments().get(1)});
                break;
            default:
                throw new UnsupportedOperationException("Fallo al actualizar "+ uri);
        }
        Context context= getContext();
        if (context != null && entradasActualizadas>0){
            context.getContentResolver().notifyChange(uri,null);
        }
        return entradasActualizadas;
    }
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (CONTENT_MATCHER.match(uri)){
            case LIST_SIZE:
            case LIST_SIZE_WITH_ID:
                return Contract.ListaEpocasPersonajes.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Tipo desconocido para "+ uri);

        }
    }
}
