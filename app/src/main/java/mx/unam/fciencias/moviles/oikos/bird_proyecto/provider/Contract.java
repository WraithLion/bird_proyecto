package mx.unam.fciencias.moviles.oikos.bird_proyecto.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class Contract {

    public static final String AUTHORITY = "mx.unam.fciencias.moviles.oikos.bird_proyecto";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://"+AUTHORITY);

    private Contract(){


    }

    public static class ListaEpocasPersonajes implements BaseColumns{

        public static final String PATH_INFINITE_LIST = "Lista_Epoca_Personaje";

        public static final Uri CONTENT_URI= BASE_CONTENT_URI.buildUpon().appendPath(PATH_INFINITE_LIST).build();

        public static final String TABLE_NAME = PATH_INFINITE_LIST;

        public static final String COLUMN_LIST_SIZE= "list_size";

        public static final byte COLUMN_INDEX_ID=0;

        public static final byte COLUMN_INDEX_LIST_SIZE=1;

        public static final String[] COLUMNS = new String[] {_ID, COLUMN_LIST_SIZE};

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE+"/"+ AUTHORITY+"/"+PATH_INFINITE_LIST;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE+"/"+AUTHORITY+"/"+PATH_INFINITE_LIST;

        public static Uri buildListSizeUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI,id);

        }

    }
}
