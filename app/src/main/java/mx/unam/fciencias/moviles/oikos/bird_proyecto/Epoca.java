package mx.unam.fciencias.moviles.oikos.bird_proyecto;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;


public class Epoca extends Navegacion {
    public static final byte RESULT_EXIT=2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar= getSupportActionBar();
        if (actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setContentView(R.layout.fragment_epocas); //Inicia el fragmento epocas

    }

}
